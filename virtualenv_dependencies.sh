#!/usr/bin/env bash

apt-get install -y gcc libxml2-dev libyaml-dev  postgresql \
    postgresql-contrib \
    python-pillow python3-pillow postgresql-server-dev-all \
    postgresql-plpython3 python3-wheel python-wheel \
    postgresql-plpython

pip3 install django-debug-toolbar djangorestframework \
            django-redis-cache django-iban-field 

